USE RecipeFinder

IF OBJECT_ID('tempdb..#InFridge') IS NOT NULL DROP TABLE #InFridge
CREATE TABLE #InFridge (IngredientId INT)
INSERT #InFridge values (1), (5), (2)

SELECT r.Name AS Recipe,  ri.IngredientId, i.Name AS Ingredient FROM Recipe r
    JOIN RecipeIngredient ri 
    ON r.RecipeId = ri.RecipeId
    JOIN Ingredient i 
    ON ri.IngredientId = i.IngredientId
WHERE ri.IngredientId = ANY
    (SELECT IngredientId FROM  #InFridge)
GROUP BY r.Name, ri.IngredientId
HAVING (ri.IngredientId IN  (SELECT i.IngredientId FROM Ingredient i WHERE i.IngredientId IN (SELECT Name FROM #InFridge)))

GO
SELECT * FROM Recipe