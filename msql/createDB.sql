USE master
GO
-- Create the new database if it does not exist already
IF NOT EXISTS (
    SELECT name
        FROM sys.databases
        WHERE name = N'RecipeFinder'
)
CREATE DATABASE RecipeFinder
GO

USE RecipeFinder;
GO

IF OBJECT_ID('dbo.Recipe', 'U') IS NOT NULL
DROP TABLE dbo.Recipe
GO
CREATE TABLE dbo.Recipe
(
    RecipeId INT NOT NULL PRIMARY KEY,
    Name [NVARCHAR](50) NOT NULL,
);
GO

IF OBJECT_ID('dbo.Ingredient', 'U') IS NOT NULL
DROP TABLE dbo.Ingredient
GO
CREATE TABLE dbo.Ingredient
(
    IngredientId INT NOT NULL PRIMARY KEY,
    Name [NVARCHAR](50) NOT NULL,
);
GO

IF OBJECT_ID('dbo.RecipeIngredient', 'U') IS NOT NULL
DROP TABLE dbo.RecipeIngredient
GO
CREATE TABLE dbo.RecipeIngredient
(
    RecipeId INT NOT NULL,
    IngredientId INT NOT NULL
);
GO

INSERT INTO Ingredient 
    (IngredientId, Name)
VALUES
    (1, 'Avacado'),
    (2, 'Tomato'),
    (3, 'Peas'),
    (4, 'Carrots'),
    (5, 'Bread')
GO

INSERT INTO Recipe
    (RecipeId, Name)
VALUES
    (1, 'Soup'),
    (2, 'Sandwich'),
    (3, 'Salad'),
    (4, 'Fry-up'),
    (5, 'Roast')
GO

SELECT * FROM Recipe

-- This is a join table
INSERT INTO RecipeIngredient
    (RecipeId, IngredientId)
VALUES
    (1, 2),
    (1, 3),
    (1, 4),
    (2, 1),
    (2, 5),
    (3, 1),
    (3, 2),
    (3, 4)
GO

----- Drop the whole database and start again
-- USE master;
-- GO
-- ALTER DATABASE RecipeFinder SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
-- GO
-- DROP DATABASE RecipeFinder;
-- GO