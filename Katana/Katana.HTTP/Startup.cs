﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Katana.HTTP.Startup))]
namespace Katana.HTTP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
