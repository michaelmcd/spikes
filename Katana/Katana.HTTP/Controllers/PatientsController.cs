﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Katana.HTTP.Models;
using MongoDB.Driver;

namespace Katana.HTTP.Controllers
{
    [Authorize]
    public class PatientsController : ApiController
    {
        private readonly IMongoCollection<Patient> _patients;

        public PatientsController()
        {
            _patients = PatientDb.Open();
        }

        [EnableCors("*","*", "GET")]
        public IEnumerable<Patient> Get()
        {
            return _patients.FindAsync(_ => true).Result.ToListAsync().Result;
        }

        public IHttpActionResult Get(int id)
        {
            var patient = _patients.Find(x => x.Id == id).FirstOrDefaultAsync().Result;
            if (patient == null)
            {
                return Content(HttpStatusCode.NotFound, "Patient not found.");
            }
            return Ok(patient);
        }

        [Route("api/patients/{id}/medications")]
        public HttpResponseMessage GetMedications(int id)
        {
            var patient = _patients.Find(x => x.Id == id).FirstOrDefaultAsync().Result;
            if (patient == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Patient not found.");
            }
            return Request.CreateResponse(patient.Mecications);
        }
    }
}