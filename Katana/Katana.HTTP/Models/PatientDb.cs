﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Katana.HTTP.Models
{
    public static class PatientDb
    {
        public static IMongoCollection<Patient> Open()
        {
            var mongoClient = new MongoClient("mongodb://localhost");
            var patientDb = mongoClient.GetDatabase("PatientDb");
            var patients = patientDb.GetCollection<Patient>("Patients");
            return patients;
        }
        public static async Task<IList<Patient>> FindAsync(Expression<Func<Patient, bool>> query)
        {
            var mongoClient = new MongoClient("mongodb://localhost");
            var patientDb = mongoClient.GetDatabase("PatientDb");
            var patients = patientDb.GetCollection<Patient>("Patients");

            // Return the enumerable of the collection
            return await patients.Find<Patient>(query).ToListAsync();
        }
    }
}