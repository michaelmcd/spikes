﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Katana.HTTP.Models
{
    public class Patient
    {
        [BsonElement("_id")]
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Medication> Mecications { get; set; }
        public ICollection<Aliment> Aliments { get; set; }
    }

    public class Medication
    {
        public string  Name { get; set; }
        public int Dose { get; set; }
    }

    public class Aliment
    {
        public string  Name { get; set; }
    }
}