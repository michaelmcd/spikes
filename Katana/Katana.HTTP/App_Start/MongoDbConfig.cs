using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Katana.HTTP.Models;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;
using MongoDB.Driver.Linq;

namespace Katana.HTTP
{
    public static class MongoDbConfig
    {
        public static void Seed()
        {
            var patientsDb = PatientDb.Open();

            var patients = PatientDb.FindAsync(_ => true);

            if (!patients.Result.Any(p => p.Name == "mark"))
            {
                var newPatients = new List<Patient>
                {
                    new Patient
                    {
                        Id = 1234,
                        Name = "mark",
                        Aliments = new List<Aliment> {new Aliment {Name = "Stress"}},
                        Mecications = new List<Medication> { new Medication {Name = "Beer"} }
                    }
                };

                patientsDb.InsertManyAsync(newPatients);
            }
        }
    }
}