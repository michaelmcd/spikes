﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.Diagnostics;
using Microsoft.Owin.Hosting;
using Owin;

namespace Katana.Console
{
    using AppFunc = Func<IDictionary<string, object>, Task>;

    public class Startup
    {
        public  void Configuration(IAppBuilder app)
        {
            app.Use(async (environment, next) =>
            {
                System.Console.WriteLine("{0}: {1}", "Path", environment.Request.Path);
                await next();
                System.Console.WriteLine("{0}: {1}", "Status code", environment.Response.StatusCode);
            });

            ConfigureWebApiHost(app);

            app.UseHelloWorldComponent();
        }

        private void ConfigureWebApiHost(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute("Default",
                "api/{controller}/{id}",
                new {id = RouteParameter.Optional});

            app.UseWebApi(config);
        }
    }

    public static class AppBuilderExtensions
    {
        public static void UseHelloWorldComponent(this IAppBuilder app)
        {
            app.Use<HelloWorldComponent>();
        }
    }
    
    public class HelloWorldComponent
    {
        private readonly AppFunc _next;

        public HelloWorldComponent(AppFunc next)
        {
            _next = next;
        }

        public Task Invoke(IDictionary<string, object> environment)
        {
            var response = environment["owin.ResponseBody"] as Stream;
            using (var writer = new StreamWriter(response))
            {
                return writer.WriteAsync("Hello World!!");
            }
        }
    }
}
