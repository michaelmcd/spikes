﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Katana.Console
{
    public class GreetingController : ApiController
    {
        public string Get()
        {
            return "Hello world.";
        }
    }
}
