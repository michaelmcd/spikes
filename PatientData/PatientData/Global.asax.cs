﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MongoDB.Driver.Linq;
using PatientData.Controllers;
using PatientData.Models;

namespace PatientData
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MongoConfig.Seed();
        }
    }

    public class MongoConfig
    {
        public static void Seed()
        {
            var patients = PatientDb.Open();

            if (!patients.AsQueryable().Any(p => p.Name == "Michael"))
            {
                var data = new List<Patient>()
                {
                    new Patient()
                    {
                        Name = "Michael",
                        Ailments = new []{new Ailment() {Name = "Fleas"}},
                        Medications = new []{new Medication {Name = "Bug Powder", Doses = 2}}
                    },
                    new Patient()
                    {
                        Name = "Anne",
                        Ailments = new []{new Ailment() {Name = "Lice"}},
                        Medications = new []{new Medication {Name = "XTra Bugz Killer", Doses = 20}}
                    },
                    new Patient()
                    {
                        Name = "Janette",
                        Ailments = new[]{new Ailment() {Name = "Rats"}},
                        Medications = new []{new Medication {Name = "Rat Poison", Doses = 1}}
                    }
                };

                patients.InsertBatch(data);
            }
        }
    }
}
