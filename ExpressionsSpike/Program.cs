﻿using System;
using System.Linq.Expressions;

namespace ExpressionsSpike
{
    public class Program
    {
        public void Main(string[] args)
        {
        }

        public static Expression<Func<Query, bool>> ExpressionBuilder(Query query)
        {
            return fun =>

                (string.IsNullOrEmpty(query.Name) || fun.Name == query.Name) &&
                (string.IsNullOrEmpty(query.Email1) || fun.Email1 == query.Email1) &&
                (string.IsNullOrEmpty(query.Email2) || fun.Email2 == query.Email2);
        }
    }

    public class Query
    {
        public string Name { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
    }
}