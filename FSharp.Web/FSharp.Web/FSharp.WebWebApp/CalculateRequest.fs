﻿namespace FSharp.WebWeb.Models

open System.ComponentModel
open System.ComponentModel.DataAnnotations

open FSharp.WebWebApp.Math

type CalculationType =
    |Add = 0
    |Multiple = 1
    |Devide = 2
    |Modulo = 3

type CalculatorRequest() =
    let mutable calculationType = CalculationType.Add
    let mutable num1 = 1
    let mutable num2 = 2

    [<Required>]