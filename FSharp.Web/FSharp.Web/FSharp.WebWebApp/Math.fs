﻿namespace FSharp.WebWebApp

module Math =

    let Add num1 num2 =
        num1 + num2

    let Multiply num1 num2 =
        num1 * num2

    let Divide num1 num2 = 
        num1 / num2

    let Modulo num1 num2 =
        num1 % num2