﻿namespace FSharp.WebWebAppTests

open FSharp.WebWebApp.Math
open Xunit
open FsUnit.Xunit

type ``Given the Maths module refrence above should be able to call its functions``() =
   
   [<Fact>] member test.
    ``when I ask Math.Add to add 1 and 1 should return 2.`` ()=
           Add 1 1 |> should equal 2

   [<Fact>] member test.
    ``when I ask Math.Multiply 1 and 1 should return 1.`` ()=
           Multiply 1 1 |> should equal 1

   [<Fact>] member test.
    ``when I ask Math.Divide 2 by 1 should return 2.`` ()=
           Add 1 1 |> should equal 2

   [<Fact>] member test.
    ``when I ask Math.Modulo 3 by 2 should return 1.`` ()=
           Add 1 1 |> should equal 1