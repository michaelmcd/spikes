let t = [1 .. 10]

let t' = t
	|> Array.filter(fun i ->  i % 2 = 0)
	|> Seq.map( fun x -> x + x)
	
t'
	