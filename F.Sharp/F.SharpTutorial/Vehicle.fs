﻿module F.SharpTutorial.Vehicle

type CarType =
    | HoverCar = 0
    | Car = 1
    | Truck = 2
    | Lorry = 3
    | Wierd = 4


type Car(color: string, wheels:int)  =
    do if wheels < 3 then failwith "That's a bycle."

    let carType =
        match wheels with
        | 3 -> CarType.HoverCar
        | 4 -> CarType.Car
        | 6 -> CarType.Truck
        | x when x % 2 = 1 -> CarType.Wierd
        | _ -> CarType.Lorry

    new() = Car("black", 4)

    member x.Move() = printf "The %s car (type: %A) is moving" color carType

