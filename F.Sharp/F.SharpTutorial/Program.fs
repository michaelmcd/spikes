﻿module F.SharpTutorial.Program
open System

[<EntryPoint>]
let main args =

    let car = Vehicle.Car("red", 4)

    car.Move()

    Console.ReadLine() |> ignore
    0

