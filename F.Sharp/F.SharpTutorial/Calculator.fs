﻿module F.SharpTutorial.MyMath

type Calculator(a:int , b:int) =
    do if a > 10 then failwith "Ten is too much"
    new () = Calculator(0,0)

    member x.Add (a,b) = a + b
    member x.Add() = a+b
    member x.Multi (a, b) = a*b
    member x.Print () = printfn "The value of a is %d and the value of b is %d" a b
