﻿module F.SharpTurorial.CarTests

open Xunit
open F.SharpTutorial

[<Fact>]
let Car_with_red_color_and_4_wheels_prints_message() =
    let redFourWheelerCar = Vehicle.Car("red", 4)    
    redFourWheelerCar.Move()
    
[<Fact>]
let Car_with_4_wheels_dose_not_fail() =
    let car = Vehicle.Car("black", 4)
    car.Move()


[<Fact>]
let Car_with_2_wheels_fails() =
    Assert.Throws<System.Exception>(fun () -> Vehicle.Car("black", 2).Move() )
