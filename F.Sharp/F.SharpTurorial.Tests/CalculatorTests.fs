﻿module F.SharpTurorial.Tests
open Xunit
open F.SharpTutorial

[<Fact>]
let add_2_2_should_equal_4() =
    let calculator = MyMath.Calculator()
    Assert.Equal(4, calculator.Add (2, 2))

[<Fact>]
let car_default_color_is_black() =
    let car = Vehicle.Car()
    car.Move()