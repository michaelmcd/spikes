﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using NUnit.Framework;

namespace Spike
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void QueryReturnsTrueIfBothEmailsAreUnique()
        {
            var query = new Query { Name = "Name1", Email1 = "Email1@foo.com", Email2 = "Email2@foo.com" };

            var expression = Program.ExpressionBuilder(query);

            // Assert.That(expression.Compile().Invoke(query), Is.True);
        }

        [Test]
        public void QueryReturnsFalseIfEitherEmailDoesNotMatch()
        {
            var query = new Query { Email1 = "Email@foo.com", Email2 = "Email@foo.com" };
            var person = new Person { Email1 = "Email1@p0.com", Email2 = "Email2@p0.com" };

            var expression = Program.ExpressionBuilder(query);

            Assert.That(expression.Compile().Invoke(person), Is.False);
        }

        [Test]
        public void QueryReturnsTrueIfEitherEmailMatches()
        {
            var query = new Query { Email1 = "Email1@p0.com", Email2 = "Email@foo.com" };
            var person = new Person { Email1 = "Email1@p0.com", Email2 = "Email2@p0.com" };

            var expression = Program.ExpressionBuilder(query);

            Assert.That(expression.Compile().Invoke(person), Is.True);
        }

        [Test]
        public void QueryReturnsFalseIfNameMatchesButNitherEmailMatch()
        {
            var query = new Query { Name = "Name1", Email1 = "Email1@foo.com", Email2 = "Email@foo.com" };
            var person = new Person { Name = "Name1", Email1 = "Email1@p0.com", Email2 = "Email2@p0.com" };

            var expression = Program.ExpressionBuilder(query);

            Assert.That(expression.Compile().Invoke(person), Is.False);
        }

        [Test]
        public void QueryReturnsTrueIfNameMatchesAndBothQueryEmailsAreEmpty()
        {
            var query = new Query { Name = "Name1" };
            var person = new Person { Name = "Name1", Email1 = "Email1@p0.com", Email2 = "Email2@p0.com" };

            var expression = Program.ExpressionBuilder(query);

            Assert.That(expression.Compile().Invoke(person), Is.True);
        }

        [Test]
        public void LinqQueryReturnsMatchingRecord()
        {
            var query = new Query { Name = "PersonX", Email1 = "Email@foo.com", Email2 = "Email@foo.com" };

            var expression = Program.ExpressionBuilder(query);
            
            var records = new List<Person>()
            {
                new Person{ Name = "Person0", Email1 = "Email1@p0.com", Email2 = "Email2@p0.com" },
                new Person{ Name = "Person1", Email1 = "Email1@p1.com", Email2 = "Email2@p1.com" },
                new Person{ Name = "Person2", Email1 = "Email1@p2.com", Email2 = "Email@p2.com" }
            }.AsQueryable();

            var matchedRecord = records.Where(expression);
            
            Assert.That(matchedRecord.Count(), Is.EqualTo(1));
        }
    }
}