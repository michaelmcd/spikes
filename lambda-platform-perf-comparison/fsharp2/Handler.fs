namespace AwsDotnetFsharp

open Amazon.Lambda.Core
open Amazon.Lambda.APIGatewayEvents
open Amazon.Lambda.APIGatewayEvents
open Amazon.Lambda.APIGatewayEvents

[<assembly:LambdaSerializer(typeof<Amazon.Lambda.Serialization.Json.JsonSerializer>)>]
do ()

module Handler =
    type Response = { message : string; response : APIGatewayProxyRequest }
    
    //let responseHandler = {message = "foogoo"; response = 200}

    let hello(request:APIGatewayProxyRequest, _:ILambdaContext): 
        APIGatewayProxyResponse = APIGatewayProxyResponse(Body = """{"message": "hello"}""", StatusCode = 200)
   