import './Menu.css';
import * as React from 'react';
import {NavLink, Link} from 'react-router-dom';

export const Menu = (props) => {
    return (
        <div className='main-nav'>
            <div className='navbar navbar-inverse'>
                <div className='navbar-brand'>Demos</div>           
                <div className='navbar-collapse collapse'>
                    <ul className='nav navbar-nav'>
                        <li>
                            <NavLink to={ '/' } exact activeClassName='active'>
                                <span className='glyphicon glyphicon-home'></span> Home
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/cards' } exact activeClassName='active'>
                                <span className='glyphicon glyphicon-education'></span> Cards
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/game' } exact activeClassName='active'>
                                <span className='glyphicon glyphicon-education'></span> Game
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );   
}
