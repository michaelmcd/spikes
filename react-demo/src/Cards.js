import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const Card = (props) => {
  return (
  <div style={{margin: '1em'}}>
    <img width='75' src={props.image} />
    <div style={{display: 'inline-block', margin: 10}}>
      <div>{props.name}</div>
      <div>{props.company}</div>
    </div>
  </div>);
}

const data = [
  {
    name: "fred",
    company: "big corp!",
    image: "https://avatars0.githubusercontent.com/u/1?v=4"
  },
  {
    name: "andy",
    company: "small corp!",
    image: "https://avatars0.githubusercontent.com/u/3300?v=4"
  },
  {
    name: "petula",
    company: "medium corp!",
    image: "https://avatars0.githubusercontent.com/u/1400?v=4"
  }
]

const Cards = (props)=> {
  return (
    <div>
      {props.data.map(cardData => <Card {...cardData} />)}
    </div>
  );
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          <Cards data={data}/>
        </p>
      </div>
    );
  }
}

export default App;
