import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import {Menu} from './Menu';
import {Game} from './Game';

class App extends Component {
  render() {
    return (
    <div className='container-fluid'>
      <div className='row'>
          <div className='col-sm-3 '>
              <Menu />
          </div>
          <div className='col-sm-9'>
              <Route path='/game' component={ Game } />
          </div>
      </div>
  </div>
    );
  }
}

export default App;
