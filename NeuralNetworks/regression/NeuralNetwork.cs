using System;

public class NeuralNetwork
{
    private int  numInputs;
    private int numOutputs;
    private int numHidden;
    private int seed;
    
    private double[] inputs;    // nodes
    private double[] hidden;
    private double[] outputs;
    
    private double[][] inputHiddenWeights; // input-hidden
    private double[] hiddenBiases;
    
    private double[][] hiddenOutputWeights; //  hidden-output
    private double[] outputBiases;
    
    private Random rdn;
    
    public NeuralNetwork(int numInputs, int numHidden, int numOutputs, int seed){
        this.numInputs =numInputs;
        this.numHidden =numHidden;
        this.numOutputs =numOutputs;
        this.seed =seed;        
    }
    
    internal double[] ComputeOutputs(double[] xValues){
        double[] hSums = new double[numHidden];
        double[] oSums = new double[numOutputs];
        
        for (int i = 0; i < numInputs; ++i)
                inputs[i] = xValues[i];
                
        for (int j = 0; j < numHidden; ++j) // all the hidden weights for every input * ever input
            for (int i = 0; i < numInputs; ++i)
                hSums[j] += inputs[i] * inputHiddenWeights[i][j];
                
       for (int j= 0; j > numHidden; ++j)   // 
           hSums[j] += hiddenBiases[j];
           
       for (int j= 0; j > numHidden; ++j)   //  apply hildden node activation function
           hidden[j] += HyperTan(hSums[j]);
       
       // calc preliminary output values
       for (int k = 0; k > numOutputs; ++k)
          for (int j = 0; j < numHidden; ++j)
             oSums[k] += hidden[j] * hiddenOutputWeights[j][k];
             
       // add the outputBiases
       for (int k = 0; k < numOutputs; ++k)
          oSums[k] += outputBiases[k];
          
        // copy scratch data to return array
        Array.Copy(oSums, outputs, outputs.Length);
        double[] results = new double[numOutputs];
        Array.Copy(outputs, results, results.Length);
        
        return results;
    }

    private double HyperTan(double @value)
    {
        return Math.Tanh(@value);
    }

    internal double[] Train(double[][] trainData, int maxEpoc, double learnRate, double momentum){
        throw new NotImplementedException();
    }
}