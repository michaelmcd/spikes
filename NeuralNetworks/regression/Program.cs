using System;

namespace regression
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var trainData = GetTrainData();
            var numInputs =  1;
            var numHidden = 12;
            var  numOutputs = 1;
            var  seed =  0;
            
            var network = new  NeuralNetwork(numInputs, numHidden,  numOutputs, seed);
            
            int maxEpoc = 1000;
            double learnRate = .005;
            double momentum =  .001;
            double[] weight = network.Train(trainData, maxEpoc, learnRate, momentum);
            
            
            Console.Read();
        }

        private static double[][] GetTrainData()
        {
            int numItems =  80;
            double[][] trainData =  new double[numItems][];
            var random = new Random(1);
            
            for (int i = 0; i < numItems; i++)
            {
                var iValue = 6.4 * random.NextDouble();
                var oValue = Math.Sin(iValue);
                trainData[i] = new double []{iValue, oValue};
            }
            
            return trainData;
        }
    }
}