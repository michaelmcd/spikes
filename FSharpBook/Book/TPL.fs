﻿module TPL

open System
open System.Collections.Concurrent
open System.Threading.Tasks

// Sort-cCreate a parallel task that accesses te LoopParellelState and cals it's Stop method:
let taskWithCancelation cancelator = 
    let bag = ConcurrentBag<_>()
    Parallel.For(0, 99999, fun i s -> if i > 10000 then bag.Add i else cancelator s) |> ignore
    (bag, bag.Count)

taskWithCancelation (fun s -> s.Stop()) |> printfn "%A"

let parallelPfn = Parallel.For(0, 20, printfn "%i") |> ignore
                  printfn "%s" "Finished 1st parallel printing of integers 0 to 19"

let parallelPfn' = Parallel.For(0, 10, sprintf "%i" >> System.Console.WriteLine) |> ignore
                   printfn "%s" "Finished 2st parallel printing of integers 0 to 9"

let parallelPfn'' = printfn "%s" "All Done!"
taskWithCancelation (fun s -> s.Stop()) |> printfn "%A"