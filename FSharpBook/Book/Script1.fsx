﻿open System

type Ratings = 
    | PG
    | R

type Movie = {Title: string; Year: int; Rating: Ratings option}

let movies = [{Movie.Title = "Terminsator"; Year=1978; Rating = Some(PG)}
              {Movie.Title = "Star Wars"; Year=1982; Rating = Some(PG)}
              {Movie.Title = "Bambie"; Year=1968; Rating = None}]

for {Title = t; Year = y; Rating = r} in movies do
    printfn "%s %i %A" t y r

