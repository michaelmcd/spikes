﻿open System
//
//type Container() =
//    member x.Fill ?stopAtPercent =
//        printfn "%s" <| match (defaultArg stopAtPercent 0.5) with 
//                        | 1.0 -> "Filled up!"
//                        | stopAt -> sprintf "Filled up to %s!" (stopAt.ToString("P2"))
//
//let bottle = Container()
 type Ratings =
    | PG
    | PG13
    | R
    | NC17

type Movie = {Title: string; Year: int; Rating: Ratings option}

let movies = [{Movie.Title = "Tompopo"; Year = 1996; Rating = Some(PG)}
              {Title = "Hunter"; Year = 1981; Rating = None}
              {Title = "Terminator"; Year = 1999; Rating = Some(R)}
              {Title = "Debie Does Dallas"; Year = 1988; Rating = Some(NC17)}]


for {Title = t; Year = y; Rating = Some(r)} in movies do
    printfn "%s (%i) - %A" t y r


(*
type Person (name: string, age: int) =
    let mutable _name: string = name
    let mutable _age: int = age
        
    new () = Person("unknown", 0)
    new (name:  string) = Person(name, 0)

    member x.Name 
        with set (value) = _name <- value
        and get () = _name
    member x.Age 
        with set (value) = _age <- value
        and get () = _age
    member x.IsAdult = _age >= 18

let square num = num * num

let numbers = [1;2;3;4;5]

let sumOfSquares numbers = 
    let mutable  sum = 0
    for num in numbers do
        sum <- sum + (square(num))
    sum

let rec sumOfSquares' numbers =
    match numbers with
    | [] -> 0
    | h::t -> square h + (sumOfSquares' t)

let rec sumOfSquares'' numbers =
    numbers
    |> Seq.map square
    |> Seq.sum 
*)
(*
let rec factorial n =
    match n with 
    | 1L -> 1L
    | _ -> n * factorial (n - 1L)

factorial 5L

let factorial' n =
    let rec fact c p = 
        match c with
        | 0 -> p
        | _ -> fact <| c - 1 <| c * p
    fact n 1

factorial' 5
*)

(*
let showOption (o: _ option) = 
    match o with
    | Some o -> o.ToString()
    | None -> "No value"
    
(showOption (Some "XYZ")) |> printfn "Option value: %s" 
(showOption (None )) |> printfn "Option value: %s" 
*)
//
//let arabicToRoman arabic = 
//    String.replicate arabic "I"
//
//arabicToRoman (2)

open System
let sleepWorkflow  = async{
    printfn "Starting sleep workflow at %O" DateTime.Now.TimeOfDay
    
    // do! means to wait as well
    do! Async.Sleep (2000)
    printfn "Finished sleep workflow at %O" DateTime.Now.TimeOfDay
    }

//test
Async.Start sleepWorkflow  

let rec count l =
    match l with
    | [] -> 0
    | h :: tail -> h + (count tail)

count [1;2;3]

let rec takeFromList n l =  
    match n, l with
    | _, [] -> []
    | 0, _ -> []
    | n, h::tail -> h :: (takeFromList (n-1) tail)

takeFromList 10 [1.. 20]

let replicate n str =
    String.replicate n str

replicate 10 "x"

