﻿module QueryExpressions

let filmActors = query {for f in QuerySource.films do
                        groupJoin fa in QuerySource.filmActors on (f.id = fa.filmId) into junction
                        select (f.name, query {for j in junction do
                                               join a in QuerySource.actors on (j.actorId = a.id)
                                               select (a.firstName, a.lastName)
                                            })
                       }                       

let printFA filmActors = filmActors
                         |> Seq.iter (fun (f, a) -> printfn "%s" f 
                                                    a 
                                                    |> Seq.iter (fun (fn, ln) -> printfn "  %s %s" fn ln))

let filmActors' = query {for f in QuerySource.films do
                         groupJoin fa in QuerySource.filmActors on (f.id = fa.filmId) into junction
                         for j in junction do
                         join a in QuerySource.actors on (j.actorId = a.id)
                         select (f.name, a.firstName, a.lastName)
                        }

let filmActorsEx = query {for f in QuerySource.films do
                          leftOuterJoin fa in QuerySource.filmActors on (f.id = fa.filmId) into junction
                          for j in junction do
                          join a in QuerySource.actors on (j.actorId = a.id)
                          select (f.name, a.firstName, a.lastName)
                        }
                        
let printFA' filmActors = filmActors
                           |> Seq.iter (fun (fname, aFirstname, aLastname)-> printfn "%s %s %s" fname aFirstname aLastname)

