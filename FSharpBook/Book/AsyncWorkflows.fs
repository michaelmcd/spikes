﻿module AsyncWorkflows

open System
open System.IO
open System.Net
open System.Text.RegularExpressions

type StreamReader with
    member x.AsyncReadToEnd () =
        async  {    do! Async.SwitchToNewThread()
                    let content = x.ReadToEnd()
                    do! Async.SwitchToThreadPool()
                    return content }

let getPage(uri: Uri) = 
    async { let req = WebRequest.Create uri
            use! response = req.AsyncGetResponse()
            use stream = response.GetResponseStream()
            use reader = new StreamReader(stream)

            return! reader.AsyncReadToEnd()}

