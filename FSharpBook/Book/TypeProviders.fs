﻿[<AutoOpen>]
module TypeProviders

open Microsoft.FSharp.Data.TypeProviders

type northwind = ODataService<"http://services.odata.org/v3/northwind/northwind.svc">

let svc = northwind.GetDataContext()

svc.DataContext.SendingRequest.Add(fun r -> printfn "%O" r.Request.RequestUri)
let invoices = query {for i in svc.Invoices do
                      select (i.CustomerName, i.Address, i.City, i.Country)
                      take 5}

let  printInv = invoices
                |> Seq.iter(fun (c, a, city,  country) ->  printfn "%s" c)
