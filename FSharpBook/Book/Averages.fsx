﻿open System

let tempratures =
    [2.1; 2.2; 4.01; 1.03]

let totalTempratures = 
    List.sum tempratures

let averageTempratures = 
    totalTempratures /  (float) tempratures.Length

printfn "Average Tempratures: %f" averageTempratures