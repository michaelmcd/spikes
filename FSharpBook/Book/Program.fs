﻿module RPNCalculator 

open System

let evalExpression (s : string) =
    let solve items current =
        match (current, items) with
        | "+", y::x::t -> (x + y)::t
        | "-", y::x::t -> (x - y)::t
        | "*", y::x::t -> (x * y)::t
        | "/", y::x::t -> (x / y)::t
        | _ -> (float current)::items
    (s.Split(' ') |> Seq.fold solve []).Head


//[<EntryPoint>]
let main argv =
    [
    "5 4 6 + /"
    "2 3 +"
    "90 34 12 33 55 66 + * - + -"
    "90 3 -"]
    |> List.map (fun expr -> expr, evalExpression expr)
    |> List.iter (fun (expr, result) -> printfn "(%s) = %A" expr result)
    Console.ReadLine() |> ignore
    0