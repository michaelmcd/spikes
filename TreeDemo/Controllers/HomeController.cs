﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TreeDemo.Models;
using TreeDemo.DAL;

namespace TreeDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {          
            List<Employee> userList = new List<Employee>();

            userList = UserDA.GetAllUsers();
            var president = userList.
                              Where(x => x.ManagerID == null).FirstOrDefault();        
            SetChildren(president, userList);

            return View(president);
        }

        private void SetChildren(Employee model, List<Employee> userList)
        {
            var childs = userList.
                            Where(x => x.ManagerID == model.ID).ToList();
            if (childs.Count > 0)
            {
                foreach (var child in childs)
                {
                    SetChildren(child, userList);
                    model.Employees.Add(child);
                }                
            }
        }

    }
}
/****************************************************
 * ASP.NET MVC3 Tree View with jsTree
 * Author  : Shyju 
 * website : http://www.techiesweb.net
 * Twitter : @kshyju
 * 
 * Thanks to jsTree, the wonderful jQuery plugin.
 * http://www.jstree.com/
****************************************************/