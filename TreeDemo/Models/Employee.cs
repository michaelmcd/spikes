﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreeDemo.Models
{
 
    
    public class Employee
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public int? ManagerID { set; get; }
        public IList<Employee> Employees { set; get; }
        public Employee()
        {
            Employees = new List<Employee>();
        }
    }


}