﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using TreeDemo.Models;
using System.Data.SqlClient;

namespace TreeDemo.DAL
{


    public class UserDA
    {
        public static List<Employee> GetAllUsers()
        {
            string connString = "Data Source=|DataDirectory|TreeDemo.sdf;";
            var userList = new List<Employee>();
            var conFactory = new SqlCeConnectionFactory("System.Data.SqlServerCe.4.0");

            using (var con = conFactory.CreateConnection(connString))
            {
                string qry="SELECT ID,Name,ManagerID FROM Users";
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = qry;
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    var reader = cmd.ExecuteReader(CommandBehavior.Default);
                    while (reader.Read())
                    {
                        var user = new Employee();
                        user.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                        user.Name = reader.GetString(reader.GetOrdinal("Name"));
                        if(!reader.IsDBNull(reader.GetOrdinal("ManagerID")))
                            user.ManagerID = reader.GetInt32(reader.GetOrdinal("ManagerID"));
                         
                        userList.Add(user);
                    }               
                }
            }
            return userList;
        }
    }



}