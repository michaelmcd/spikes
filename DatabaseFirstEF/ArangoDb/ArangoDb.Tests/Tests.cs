﻿using Arango.Client;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace ArangoDb.Tests
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void SetUp()
        {
            // adds new connection data to database manager
            ASettings.AddConnection(
                "Offers.Core",
                "172.28.128.3",
                8529,
                false,
                "offers_core",
                "root",
                "T77wJ5MeHKJk979N"
            );
        }

        [Test]
        public void IsConnectionStoredUnderAlias()
        {
            var hasConnection = ASettings.HasConnection("Offers.Core");

            Assert.IsTrue(hasConnection);

            ASettings.RemoveConnection("Offers.Core");
        }

        [Test]
        public void DocumentShoulNotExist()
        {
            var db = new ADatabase("Offers.Core");

            var response = db.Document.Get("DealCollection/123");

            Assert.That(response.Error.StatusCode == 404);
        }

        [Test]
        public void CollectionShouldExist()
        {
            var db = new ADatabase("Offers.Core");

            var response = db.Collection.Get("DealCollection");

            Assert.That(response.StatusCode == 200);
        }

        [Test]
        public void CollectionShouldNotExist()
        {
            var db = new ADatabase("Offers.Core");

            var response = db.Collection.Get("FooBar_Collection");

            Assert.That(response.Error.StatusCode == 404);
        }
    }
}