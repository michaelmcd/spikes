﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using DatabaseFrist.Mvc.Model;

namespace DatabaseFrist.Mvc.Contorlers
{
    public class ProductController : Controller
    {
        private readonly NutshellDB db = new NutshellDB();

        //[Route("Product")]
        public async Task<ActionResult> Index()
        {
            return View(await db.Products.ToListAsync());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}