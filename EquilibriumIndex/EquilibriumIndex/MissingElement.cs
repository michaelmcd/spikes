﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace EquilibriumIndex
{
    [TestFixture]
    public class MissingElement
    {
        [TestCase(new int[] { 4, 3, 1, 5 }, 2)]
        [TestCase(new int[] { 4, 3, 2, 5 }, 1)]
        [TestCase(new int[] { 4, 3, 1, 5, 7, 2 }, 6)]
        [TestCase(new int[]{}, 1)]
        [TestCase(new int[]{2}, 1)]
        [TestCase(new int[]{1}, 2)]
        public void Given_this_should_do(int[] array, int expectedElement)
        {
            int missing = FindMissing(array);

            Assert.That(missing, Is.EqualTo(expectedElement));
        }

        private int FindMissing(int[] array)
        {
            var temp = new bool[array.Length  + 1];
            int missing = 1;
        
            foreach(int i in array)
            {
                temp[i - 1] = true;
            }

            for (int i = 0; i < temp.Length ; i++)
            {
                if (temp[i] == false)
                {
                    missing = i + 1;
                    break;
                }
            }

            return missing;
        }
    }
}
