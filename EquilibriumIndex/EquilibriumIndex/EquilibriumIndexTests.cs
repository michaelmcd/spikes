﻿using System.Linq;
using NUnit.Framework;

namespace EquilibriumIndex
{
    [TestFixture]
    internal class EquilibriumIndexTests
    {
        [TestCase(new[] {-7, 1, 5, 2, -4, 3, 0}, 6)]
        [TestCase(new[] {-7, 1, 5, 2, -4, 3, 1}, 6)]
        [TestCase(new[] {1, 2, 0, 1, 2}, 2)]
        [TestCase(new[] {1, 2, 0, 3, 0}, 2)]
        [TestCase(new[] {1, 2, 0, 4, -1}, 2)]
        public void Given_an_array_of_integers_should_the_equilibrum_index(int[] array, int expectedIndex)
        {
            int actualIndex = FindEIndex(array);

            Assert.That(actualIndex, Is.EqualTo(expectedIndex));
        }

        private int FindEIndex(int[] array)
        {
            int left = array.Sum();
            int right = 0;

            for (int index = array.Length - 1; index >= 0; index--)
            {
                left -= array[index];
                if (left == right)
                {
                    return index;
                }
                right += array[index];
            }

            return -1;
        }
    }
}