﻿using System;
using System.Linq;
using NUnit.Framework;

namespace EquilibriumIndex
{
    [TestFixture]
    public class MinimalDiffrence
    {
        [Test]
        public void Given_an_array_should_mimimal_diffrece_between_split_array()
        {
            int minDif = FindMinDif(new int[] { 3, 1, 2, 4, 3 });

            Assert.That(minDif, Is.EqualTo(1));
        }

        [TestCase(new int[] { 3, 1, 2, 4, 3 }, 1)]
        [TestCase(new int[] { 3, 1, 2, 3, 3 }, 0)]
        [TestCase(new int[] { 3, 3, 3, 3, -3 }, 3)]
        public void Given_an_multiple_arrays_should_mimimal_diffrece_between_split_arrays(int[] arrray, int expectedMinDif)
        {
            int minDif = FindMinDif(arrray);

            Assert.That(minDif, Is.EqualTo(expectedMinDif));
        }

        private int FindMinDif(int[] arrayInts)
        {
            int right = arrayInts[arrayInts.Length - 1];
            int left = arrayInts.Sum() - arrayInts[arrayInts.Length -1] ;
            int minDiff = Math.Abs(left - right);

            for (int i = arrayInts.Length - 2 ; i > 0; i--)
            {
                var diff = Math.Abs(left - right);

                if ( minDiff > diff  )
                {
                    minDiff = diff;
                }

                left -= arrayInts[i];
                right += arrayInts[i];
            }

            return minDiff;
        }
    }
}