﻿using NUnit.Framework;

namespace EquilibriumIndex
{
    [TestFixture]
    class FrogJumpTests
    {
        [Test]
        public void Given_this_should_do()
        {
            int minJumps = MinJumps(10, 85, 30);

            Assert.That(minJumps, Is.EqualTo(3));
        }

        private int MinJumps(int start, int end, int jump)
        {
            int distance = (end - start);
            int minJumps = (distance/jump);

            if (minJumps * jump == distance)
            {
                return minJumps;
            }

            return minJumps + 1;
        }
    }
}
