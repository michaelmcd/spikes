﻿using System;
using System.Linq;

namespace Fibonacci
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    public class Program
    {
        public static void Main()
        {
            // 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
            TestFib();
        }

        static IEnumerable<int> fib()
        {
            var first = 0;
            var second = 1;
            yield return first;
            yield return second;

            while (true)
            { 
                (first, second) = (second, second + first);
                yield return second;
            }
        }

        static void TestFib()
        {
            var expectedFibs = new int[] { 0, 1, 1, 2, 3, 5, 8, 13 };
            var count = expectedFibs.Length;

            var actualFibs = fib().Take(count);

            for (int i = 0; i < count; i++)
            {
                if (!Equal(expectedFibs[i], actualFibs.ElementAt(i)))
                {
                    Console.WriteLine($"Expected fib: {expectedFibs[i]} not equal to actual fib: {actualFibs.ElementAt(i)}");
                }
                //Console.WriteLine(actualFibs.ElementAt(i));
            }
            Console.WriteLine(".....");
            Console.WriteLine("End");
            Console.ReadKey();
        }

        static bool Equal(int a, int b)
        {
            return a == b;
        }
    }
}
