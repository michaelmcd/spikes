﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fibonacci
{
    public class FibGen
    {
        public static IEnumerable<int> fibber()
        {
            int first = 0;
            int second = 1;

            yield return first;
            yield return second;
            while (true)
            {
                (first, second) = (second, second + first);
                yield return second;
            }
        }
    }
}
