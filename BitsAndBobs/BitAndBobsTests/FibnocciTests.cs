using System;
using Xunit;
using Fibonacci;
using System.Linq;

namespace BitAndBobsTests
{
    public class FibnocciTests
    {
        /// <summary>
        ///  0, 1, 1, 2, 3, 5, 8
        /// </summary>
        [Theory]
        [InlineData(7, 8)]
        public void TestFibnocci(int nth, int expectedValue)
        {
            var fib = Fibonacci.FibGen.fibber().Take(7);

            Assert.Equal(expectedValue, fib.Last());
        }
    }
}
