﻿using System;
using CommonDominator;
using Xunit;

namespace BitAndBobsTests
{
    public class HighestCommonDenominatorTests
    {
        [Theory]
        [InlineData(2, 4, 2)]
        [InlineData(12, 8, 4)]
        [InlineData(48, 18, 6)]
        [InlineData(84, 18, 6)]
        [InlineData(48, 180, 12)]
        [InlineData(48, 180, 9)]
        public void Hcd_given_two_numbers_retrun_the_hcd(int number_1, int number_2, int expectedHcd)
        {
            IHCD hcd = new EuclidsHCD();
            int result = hcd.Calculate(number_1, number_2);
            Assert.Equal(expectedHcd, result);
        }
    }
}
