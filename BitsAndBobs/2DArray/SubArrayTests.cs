﻿using System;
using System.Linq;
using NUnit.Framework;

namespace _2DArray
{
    [TestFixture]
    public class SubArrayTests
    {
        [Test]
        public void Given_tiny_2D_array_of_1s_and_0s_should_find_subarray_of_1s()
        {
            // arrange
            bool[,] array =
            {
                {true, true, false},
                {true, true, false}
            };

            var arrayAnalyser = new ArrayAnalyser();

            // act
            bool[,] subArray = arrayAnalyser.GetSubArray(array);

            // assert
            Assert.That(subArray, Is.EqualTo(new[,]
            {
                {true, true},
                {true, true}
            }));
        }

        [Test]
        public void Given_2D_array_of_1s_and_0s_should_find_subarray_of_1s()
        {
            // arrange
            bool[,] array =
            {
                {true, true, false},
                {true, true, false},
                {true, true, false}
            };

            var arrayAnalyser = new ArrayAnalyser();

            // act
            bool[,] subArray = arrayAnalyser.GetSubArray(array);

            // assert
            Assert.That(subArray, Is.EqualTo(new[,]
            {
                {true, true},
                {true, true}
            }));
        }
    }

    public static class ArrayHelpers
    {
        public static void Populate<T>(this T[,] array, T value)
        {
            for (int row = 0; row < array.GetLength(0); row++)
            {
                for (int col = 0; col < array.GetLength(1); col++)
                {
                    array[row, col] = value;
                }
            }
        }
    }

    public class ArrayAnalyser
    {
        private const int TRUE_CELL_VALUE = 1;
        private const int FALSE_CELL_VALUE = 0;

        public bool[,] GetSubArray(bool[,] array2D)
        {
            var subArrayScores = ComputeSubArrayScores(array2D);

            var subArray = CreateSubArrayBasesOnMaxValueInArray(subArrayScores);

            return subArray;
        }

        private static bool[,] CreateSubArrayBasesOnMaxValueInArray(int[,] subArrayScores)
        {
            var maxSquareSubArrayDimension = subArrayScores.Cast<int>().Max();

            var subArray = new bool[maxSquareSubArrayDimension, maxSquareSubArrayDimension];
            subArray.Populate(true);

            return subArray;
        }

        private int[,] ComputeSubArrayScores(bool[,] array2D)
        {
            var subArrayScores = CreateSubArrayScoreTrackingArray(array2D);

            for (int row = array2D.GetLength(0) - 1; row >= 0; row--)
                for (int col = array2D.GetLength(1) - 1; col >= 0; col--)
                {
                    // complute the cells value from surrounding cells
                    int cellValue = ComputeCellValue(array2D, subArrayScores, row, col);
                    subArrayScores[row, col] = cellValue;
                }
            return subArrayScores;
        }

        private static int[,] CreateSubArrayScoreTrackingArray(bool[,] array2D)
        {
            return new int[array2D.GetLength(0), array2D.GetLength(1)];
        }

        private int ComputeCellValue(bool[,] array, int[,] results, int row, int col)
        {
            if (IsCellValueFalse(array[row, col]))
            {
                return FALSE_CELL_VALUE;
            }

            if (IsCellOnBottomRow(array, row))
            {
                return TRUE_CELL_VALUE;
            }

            if (IsCellOnRightMostColumn(array, col))
            {
                return TRUE_CELL_VALUE;
            }

            int valueBasedOnSurroundingCellValues = TRUE_CELL_VALUE + MinValueOfSurroundingCells(results, row, col);

            return valueBasedOnSurroundingCellValues;
        }

        private static int MinValueOfSurroundingCells(int[,] results, int row, int col)
        {
            return (new [] { results[row, col + 1], results[row + 1, col], results[row + 1, col + 1] }).Min();
        }

        private static bool IsCellOnRightMostColumn(bool[,] array, int col)
        {
            return array.GetLength(1) - 1 == col;
        }

        private static bool IsCellOnBottomRow(bool[,] array, int row)
        {
            return array.GetLength(0) - 1 == row;
        }

        private static bool IsCellValueFalse(bool cellValue)
        {
            return !cellValue;
        }
    }
}