﻿using System;

namespace CommonDominator
{
    public interface IHCD
    {
        int Calculate(int number_1, int number_2);
    }

    public class EuclidsHCD : IHCD
    {
        public int Calculate(int number_1, int number_2)
        {
            int biggest = Math.Max(number_1, number_2);
            int smallest = Math.Min(number_1, number_2);

            int remainder = biggest % smallest;
            if (remainder == 0)
            {
                return smallest;
            }

            var result = Calculate(smallest, remainder);

            return result;
        }
    }
}
