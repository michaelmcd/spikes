﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EFApplication.Startup))]
namespace EFApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
