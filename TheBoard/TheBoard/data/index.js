﻿(function (data) {
    var seedData = require("./seedData");
    var database = require("./database");
    
    data.getUser = function (username, next) {
        database.getDb(function (err, db) {
            if (err) {
                return next(err);
            } else {
                db.users.findOne({ username: username }, next);
            }
        });
    };
    
    data.getNoteCategories = function (next) {
        database.getDb(function (err, db) {
            if (err) {
                return next(err, null);
            } else {
                db.notes.find({ category: null }).toArray(function (err, notes) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, notes);
                    }
                });
            }
        });
    };
    
    data.getNotes = function (categoryName, next) {
        database.getDb(function (err, db) {
            if (err) {
                return next(err, null);
            } else {
                db.notes.findOne({ name: categoryName }, next);
            }
        });
    }
    
    data.createNewCategory = function (categoryName, next) {
        database.getDb(function (err, db) {
            if (err) {
                return next(err);
            } else {
                db.notes.find({ name: categoryName }).count(function (err, count) {
                    if (err) {
                        return next(err);
                    } else {
                        if (count !== 0) {
                            return next("Category already existe.");
                        }
                        var cat = {
                            name: categoryName,
                            notes: []
                        };
                        db.notes.insert(cat, function (err) {
                            if (err) {
                                console.log("Could not insert category: " + err);
                                return next(err);
                            } else {
                                return next(null);
                            }
                        });
                    }
                });
            }
        });
    };
    
    data.addUser = function(user, next){
        database.getDb(function (err, db) {
            if (err) {
                console.log("Could not connect to db: " + err);
            } else {
                db.users.insert(user, next);
            }
            
        });
    };
    
    data.createNote = function (category, note, next) {
        database.getDb(function (err, db) {
            if (err) {
                console.log("Could not connect to db: " + err);
            } else {
                db.notes.update({ name: category }, {$push: {notes: note}}, next);
            }
        })
    };

    function seedDatabase() {
        database.getDb(function (err, db) {
            if (err) {
                console.log("Could not connect to db: " + err);
            } else {
                db.notes.count(function (err, count) {
                    if (err) {
                        console.log("Could not get notes from the db: " + err);
                    } else {
                        if (count == 0) {
                            seedData.initialNotes.forEach(function (note) {
                                db.notes.insert(note, function (err) {
                                    if (err) console.log("Could not insert notes: " + err);
                                });
                            });
                        } else {
                            console.log("Database already seeded.");
                        }
                    }
                });
            }
        });
    };
    
    seedDatabase();
})(module.exports)