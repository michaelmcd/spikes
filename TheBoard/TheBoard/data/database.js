﻿(function(database) {
    var mongodb = require("mongodb");
    var mongodbUrl = "mongodb://localhost:27017/theBoard";
    var theDb = null;

    database.getDb = function(next) {
        if (!theDb) {
            mongodb.MongoClient.connect(mongodbUrl, function(err, db) {
                if (err) { 
                    return next(err, null);
                } else {
                    theDb = {
                        db: db,
                        notes: db.collection("notes"),
                        users: db.collection("users")
                    };
                    return next(null, theDb);
                }
            });
        } else {
            return next(null, theDb); 
        }
    }
})(module.exports)