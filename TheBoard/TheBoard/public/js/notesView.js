﻿(function (angular) {
    var theModule = angular.module("notesView", ["ui.bootstrap"]);
    
    theModule.controller("notesViewController", 
        ["$scope", "$window", "$http",
        function ($scope, $window, $http) {
            $scope.notes = [];
            var urlParts = $window.location.pathname.split("/");
            var category = urlParts[urlParts.length - 1];
            
            var notesUrl = "/api/notes/" + category;
            $http.get(notesUrl)
                .then(function (result) {
                $scope.notes = result.data;
            }, function (err) {
                alert(err);
            });

            $scope.newNote = createNewNote();

            $scope.save = function () {
                $http.post(notesUrl, $scope.newNote)
                    .then(function (result) {
                        $scope.notes.push(result.data);
                        $scope.newNote = createNewNote();
                    }, function (err) {
                        // error TODO
                    });
            };
        }
    ]);

    function createNewNote(){
        return {
            note: "",
            color: "yellow"
        };
    };

})(window.angular);