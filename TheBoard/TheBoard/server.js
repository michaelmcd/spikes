﻿var http = require("http");
var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var flash = require("connect-flash");

var app = express();
app.set("view engine", "vash");

app.use(express.static(__dirname + "/public"));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser("keyboard cat"));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {}
}));
app.use(flash());

var auth = require("./auth");
auth.init(app);

var controllers = require("./controllers");
controllers.init(app);

app.get("/users", function(req, res) {
    res.set("content-type", "application/json");
    res.send({ name: "michael", age: 55, isValid: true });
});

var server = http.createServer(app);
server.listen(3000);