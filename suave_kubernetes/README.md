# suavecore
A sample application running suave on dotnet core on kubernetes

Build the docker container
`docker build . -t michaelmcd/suave_kubernetes:v1.1`

Test it
`docker run -p=8083:8083 --name suave michaelmcd/suave_kubernetes:v1.1 --ip 0.0.0.0`

Push it to Docket Hub
`docker push  michaelmcd\suave_kubernetes` or your own Docker Hub repo

Set up Google Cloud (need to register on [Google Cloud](https://cloud.google.com/) and download the [SDK](https://cloud.google.com/sdk/)).

Create a cluster called k1 
`gcloud container clusters create k1`

Install *kubectl* CLI for working with Google Cloud
`gcloud components install kubectl`

Now run the following from inside the _/kubernetes/_ folder:

Create some keys for the *nginx* _secret_ which we'll need later
`openssl req -newkey rsa:4096 -nodes -sha512 -x509 -days 3650 -nodes -out tls/cert.pem -keyout tls/key.pem`

Register the *nginx* _frontend.conf_ configuration
`kubectl create configmap nginx-frontend-conf --from-file nginx/frontend.conf` 

Create a Kubernetes _deployment_ for nginx
`kubectl create -f deployments/frontend.yaml`

Create one for the *hello* app
`kubectl create -f deployments/hello.yaml`

Now we create the infrastructure _services_ to load-balance the three app instance we've deployed above

`kubectl create -f services/frontend.yaml`

Finaly wire up the app to the frontend service 
`kubectl create -f services/hello.yaml`

To get a status report on the overall deployment progress run
`kubectl get services`

That's it.
