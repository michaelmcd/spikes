using System;
using Xunit;
using RomanNumeralsKata;

namespace RomanNumeralsTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(1, "i")]
        public void Test1(int input, string expected) =>        
            Assert.Equal(expected, string.Empty);        
    }
}
