﻿module Reader

open System
open System.Linq
open System.Text

let file = @"C:\temp\data.txt"

let data = List.ofSeq <| IO.File.ReadLines(file) 

let parseLine (ln:string) =
    let bits = List.ofArray(ln.Split(' '))
    match bits with
    | name::num:: _ -> (name, Int32.Parse(num))
    | _ ->  failwith  "bad data"

let parsedLines lines =
    lines
    |> List.map parseLine
    
let rec sumOf parsedData =
    match parsedData with
    | [] ->0
    | (_ ,  value)::tail -> 
        let sumOfTail = sumOf tail
        value + sumOfTail

let calculatePercentage n total =
    float(n) / float(total) * 100.00
        
