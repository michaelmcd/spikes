﻿open System
open System.Linq

[<EntryPoint>]
let main argv = 
    
    let parsedData = Reader.parsedLines (Reader.data)

    let  printTupel  =
        function
        | (name, value) ->  printfn "%A %A" name value

    let print tuples =
        tuples
        |> List.iter printTupel
        
    print parsedData

    printfn "Sum of values: %A" (Reader.sumOf parsedData)

    Console.ReadKey() |>  ignore
    0